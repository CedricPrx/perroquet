#include <stdio.h>
#include <stdlib.h>

#include "proto.h"


// Fonction de cryptage par perroquet

void crypt()
{
//Chemin des fichiers m

    char pathR[PD+FN+1] = "./R_file/";
    char pathX[PD+FN+1] = "./X_file/";
    char pathK[PD+FN+1] = "./K_file/";

//Ouverture du fichier source � chiffrer

    FILE* file_source = NULL;

    char filename_source[FN+1];
    printf("Enter source filename to crypt (max %d characters) : ", FN);
    scanf(" %s", filename_source);

    strcat(pathR,filename_source);

    file_source = fopen(pathR, "rt");
    if (file_source == NULL)
    {
        printf("\nError at opening of %s !!\n", filename_source);
        return EXIT_FAILURE;
    }

//Cr�ation du fichier chiffr�

    FILE* file_dest = NULL;

    char filename_dest[FN+1];
    printf("\nEnter encrypted filename where to save (max %d characters) : ", FN);
    scanf(" %s", filename_dest);

    strcat(pathX,filename_dest);

    file_dest = fopen(pathX, "wt");
    if (file_dest == NULL)
    {
        printf("\nImpossible to create %s file !!\n", filename_dest);
        return EXIT_FAILURE;
    }

//Lecture ou cr�ation du fichier avec le perroquet

    FILE* file_perroq= NULL;

    char filename_perroq[FN+1];
    printf("\nEnter parrot key filename (max %d characters) : ", FN);
    scanf(" %s", filename_perroq);

    strcat(pathK,filename_perroq);

    file_perroq = fopen(pathK, "rt");
    if (file_perroq == NULL)
    {
        perroq_gen(pathK,filename_perroq);

        file_perroq = fopen(pathK, "rt");


        if (file_perroq == NULL)
        {
            printf("\nError at opening of %s !!\n", filename_perroq);
            return EXIT_FAILURE;
        }
    }

//Chiffrage

    char lettre_perroq;
    char lettre_source;
    char lettre_dest;

    //Chiffrage du premier caract�re pour v�rifier si fichiers vides

    fread(&lettre_source, sizeof(char), 1, file_source);
    if (feof(file_source))
    {
        printf("\nFile %s empty !!!\n", filename_source);
        return EXIT_FAILURE;
    }

    fread(&lettre_perroq, sizeof(char), 1, file_perroq);
    if (feof(file_perroq))
    {
        printf("\nFile %s empty !!!\n", filename_perroq);
        return EXIT_FAILURE;
    }

    lettre_dest = lettre_source - lettre_perroq;
    fwrite(&lettre_dest, sizeof(char), 1, file_dest);

    //Chiffrage des caract�res suivants jusqu'� la fin du fichier source

    while(!feof(file_source)) //TANT QUE NON FIN DE FICHIER
    {
        fread(&lettre_source, sizeof(char), 1, file_source);
        if(feof(file_source)) break;

        fread(&lettre_perroq, sizeof(char), 1, file_perroq);
        if(feof(file_perroq))
        {
            rewind(file_perroq);
            fread(&lettre_perroq, sizeof(char), 1, file_perroq);
        }

        lettre_dest = lettre_source - lettre_perroq;

        fwrite(&lettre_dest, sizeof(char), 1, file_dest);
    }

    printf("\nFile %s successfully encrypted in %s !!!\n", filename_source, pathX);

//Fermeture de tous les fichiers

    if (fclose(file_source)!= 0)
    {
        printf("\nError at closing of %s !!\n", filename_source);
        return EXIT_FAILURE;
    }
    if (fclose(file_dest)!= 0)
    {
        printf("\nError at closing of %s !!\n", filename_dest);
        return EXIT_FAILURE;
    }
    if (fclose(file_perroq)!= 0)
    {
        printf("\nError at closing of %s !!\n", filename_perroq);
        return EXIT_FAILURE;
    }

//Suppression fichier source

    char rm;
    printf("\nDo you want to erase %s ? (y/n) : ", filename_source);
    scanf(" %c", &rm);
    if(rm == 'y')
    {
        if(remove(filename_source) != 0)
        {
            printf("\nError at erasing of %s !! \n", filename_source);
            printf("Check it in %s\n", pathR);
            return EXIT_FAILURE;
        }
        else
        {
            printf("\nFile %s successfully erased !!!\n", filename_source);
        }
    }
    else
    {
        printf("\nSource file %s readable !!! Think to erase it later !!!\n", filename_source);
    }
}
