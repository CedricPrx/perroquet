#include <stdio.h>
#include <stdlib.h>

#include "proto.h"


int main()
{
    char choice;

    printf("Encryption / Decryption with a parrot key file\n"
           "\nMake sure to have directories and files in: \n"
           "    ./R_file for unencrypted files \n"
           "    ./X_file for encrypted files \n"
           "    ./K_file for parrot key files \n");

    printf("\nContinue ? ('q' to abort) : ");
    scanf(" %c", &choice);

// Menu

    while(choice != 'q')
    {
        printf("\nWhat do you want to do ? \n"
                "c - Crypt\n"
                "d - Decrypt\n"
                "q - Exit\n"
                "\nEnter your letter choice : ");

        scanf(" %c", &choice);

        switch(choice)
        {
            case 'c' :
                printf("\nStarting crypting processus \n\n");
                crypt();
                break;

            case 'd' :
                printf("\nStarting decrypting processus \n\n");
                decrypt();
                break;

            case 'q' :
                printf("\nHave a nice day !!\n");
                break;

            default :
                printf("\nChoice %c doesn't exist !! \nChoose again\n", choice);
                break;
        }
    }

    return 0;
}


