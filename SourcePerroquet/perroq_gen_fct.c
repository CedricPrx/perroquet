#include <stdio.h>
#include <stdlib.h>

#include "proto.h"

// Generation d'un perroquet dans un fichier

void perroq_gen(char* pathK, char* filename)
{
    FILE* file_perroq = NULL;

    printf("Generate a parrot key file\n");

    file_perroq = fopen(pathK, "w+t");
    if (file_perroq == NULL)
    {
        printf("\nImpossible to create %s file !!\n", filename);
        return EXIT_FAILURE;
    }

    int key_s;
    printf("Enter parrot max key size for crypting : ");
    scanf(" %d", &key_s);

    char key[key_s+1];
    printf("Enter parrot key phrase : \n");
    scanf(" %s", key);
    fprintf(file_perroq, "%s", key);

    printf("\nParrot key registered in %s\n", pathK);

    if (fclose(file_perroq)!= 0)
    {
        printf("\nError at closing of %s !!\n", filename);
        return EXIT_FAILURE;
    }
}
