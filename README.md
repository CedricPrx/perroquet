On souhaite à partir d’un fichier source, faire en sorte de le crypter en utilisant l’algorithme du 
« perroquet ».

Cet algorithme repose sur un mot (le perroquet) qui permet de crypter en calculant la différence 
ASCII caractère par caractère.

Illustration du principe du perroquet :

Mot source à crypter : Olivier
Perroquet : abcd
Résultat en calcul ascii :
(‘O’-‘a’)(‘l’-‘b’)(‘i’-‘c’)(‘v’-‘d’)(‘i’-‘a’)(‘e’-‘b’)(‘r’-‘c’)
Résultat : Concaténation des codes ASCII

L’utilisateur devra lui-même définir son propre perroquet.
Celui qui crypte et qui décrypte doit donc connaître le mot (ou la phrase) du « perroquet ». 
On prévoira donc un fichier (« peroq.def ») contenant la chaine de caractères du perroquet.

On disposera de 2 fichiers.
Source (« source.txt ») : contenant le texte à crypter
Résultat (« dest.crt ») : contenant le texte crypté

De plus, on prévoira de supprimer la source après cryptage de telle manière à ne conserver que le fichier crypté.

Afin de faciliter l’utilisation de votre programme, vous proposerez à l’utilisateur un menu simple et convivial implémentant l’ensemble des fonctionnalités.

A l’issue de la durée du projet, vous déposerez dans la section "Travaux", le projet dans sa globalité. Vous y mettrez le fichier zippé de votre projet ainsi que tous les fichiers nécessaires à son exécution. Vous mettrez également un fichier texte contenant le lien vers votre dépôt distant.

CONSEIL : En vue de faciliter la réutilisation des éléments, créer des fonctions indépendantes en leur définissant pour chacune un rôle bien précis.
